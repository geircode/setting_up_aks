FROM python:3.6

WORKDIR /app
COPY . /app

RUN pip install -r requirements.txt 

RUN apt-get update
RUN apt-get install -y jq dos2unix openssl

# Makes it possible to run Docker Commands from inside the Container to the Docker Host
RUN curl -fsSLO https://get.docker.com/builds/Linux/x86_64/docker-17.03.1-ce.tgz && tar --strip-components=1 -xvzf docker-17.03.1-ce.tgz -C /usr/local/bin

WORKDIR /app
COPY . /app

# RUN pip install -r requirements.txt 
RUN find . -type f -print0 | xargs -0 dos2unix

ENTRYPOINT tail -f /dev/null