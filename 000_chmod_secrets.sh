#!/bin/sh

rm -fr /secrets 
mkdir /secrets
cp -r /run/secrets/. /secrets/.
find /secrets -type f -exec chmod 0400 {} \;

