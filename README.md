# Setting up a container based development plattform to setup AWS EKS with Terraform

Usage:

- Run *Dockerfile.build.bat* first if you want to create your own Container Image.
- Run *docker-compose.up.bat* to start the workspace container

Install Azure CLI:

`curl -L https://aka.ms/InstallAzureCli | bash`

Then just press enter a lot of times to accept all defaults

To login:

`az login`