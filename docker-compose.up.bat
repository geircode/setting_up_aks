REM Run Dockerfile.build.bat first if you want to create your own Container Image.
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build --remove-orphans
REM Press a key to continue
pause
docker exec -it setting_up_aks-1 /bin/bash